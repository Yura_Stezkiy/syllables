﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Helpers
{
    public static class ProcessorType
    {
        private static string processorType;
        public static string ProcessorTypeProperty
        {
            get { return processorType; }
            set { processorType = value; }
        }
    }
}
