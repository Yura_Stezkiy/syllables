﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Sklady.Models;
using Sklady.TextProcessors;
using Core.Models;

namespace Sklady.TextProcessors
{
    public class EnglishPhoneticProcessor : PhoneticProcessorBase
    {
        private string[] dzPrefixes = new string[] { "під", "над", "від" };

        public EnglishPhoneticProcessor ( CharactersTable charactersTable, bool[] isCheckbox )
            : base(charactersTable, isCheckbox)
        {
        }

        public override string Process ( string input, bool[] isCheckbox )
        {
            string preparedForSyllableCount = " " + input + " ";
            string res = SyllablesReplace(preparedForSyllableCount);

            return res;
        }
        public string SyllablesReplace(string word)
        {
            string initialWord = word;

            if(!ContainsVowelFull(word)) //rule 7
            {
                word = "Г";
            }
    
            word = Regex.Replace(word, "cionally ", "ГГГ "); //rule 8
            word = Regex.Replace(word, "sionally ", "ГГГ ");
            word = Regex.Replace(word, "tionally ", "ГГГ ");
            word = Regex.Replace(word, "cionals ", "ГГ "); // rule 9
            word = Regex.Replace(word, "sionals ", "ГГ ");
            word = Regex.Replace(word, "tionals ", "ГГ ");
            word = Regex.Replace(word, "cionals ", "ГГГ ");
            word = Regex.Replace(word, "sually ", "ГГГ "); // rule 10
            word = Regex.Replace(word, "tually ", "ГГГ ");
            word = Regex.Replace(word, "cional ", "ГГ "); // rule 11
            word = Regex.Replace(word, "sional ", "ГГ ");
            word = Regex.Replace(word, "tional ", "ГГ ");
            word = Regex.Replace(word, "ions ", "Г "); //rule 12
            word = Regex.Replace(word, "nuals ", "ГГ "); //rule 13
            word = Regex.Replace(word, "suals ", "ГГ ");
            word = Regex.Replace(word, "tuals ", "ГГ ");
            word = Regex.Replace(word, " were ", " Г "); //rule 14
            if (ConsonantFront(word, "ake ")) //rule 15
            {
                word = Regex.Replace(word, "ake ", "Г ");
            }
            word = Regex.Replace(word, "nual ", "ГГ "); //rule 16
            word = Regex.Replace(word, "sual ", "ГГ ");
            word = Regex.Replace(word, "tual ", "ГГ ");
            word = Regex.Replace(word, "cion ", "Г "); //rule 17
            word = Regex.Replace(word, "sion ", "Г ");
            word = Regex.Replace(word, "tion ", "Г ");

            if (!ConsonantFront(word, "les ")) //rule 18
            {
                word = Regex.Replace(word, "les ", "Г ");
            }
            if (!ConsonantFront(word, "le "))
            {
                word = Regex.Replace(word, "le ", "Г ");
            }

            if (!ConsonantFront(word, "mes ")) //rule 19
            {
                word = Regex.Replace(word, "mes ", "Г ");
            }
            if (!ConsonantFront(word, "me "))
            {
                word = Regex.Replace(word, "me ", "Г ");
            }

            if (!ConsonantFront(word, "ves ")) //rule 20
            {
                word = Regex.Replace(word, "ves ", "Г ");
            }
            if (!ConsonantFront(word, "ve "))
            {
                word = Regex.Replace(word, "ve ", "Г ");
            }

            if (!ConsonantFront(word, "nes ")) //rule 21
            {
                word = Regex.Replace(word, "nes ", "Г ");
            }
            if (!ConsonantFront(word, "ne "))
            {
                word = Regex.Replace(word, "ne ", "Г ");
            }

            word = Regex.Replace(word, "opes ", "Г "); //rule 22
            word = Regex.Replace(word, "ope ", "Г ");
            word = Regex.Replace(word, "ease ", "Г "); //rule 23

            if (!ConsonantFront(word, "ses ")) //rule 24
            {
                word = Regex.Replace(word, "ses ", "Г ");
            }
            if (!ConsonantFront(word, "se "))
            {
                word = Regex.Replace(word, "se ", "Г ");
            }

            if (!ConsonantFront(word, "des ")) //rule 25
            {
                word = Regex.Replace(word, "des ", "Г ");
            }
            if (!ConsonantFront(word, "de "))
            {
                word = Regex.Replace(word, "de ", "Г ");
            }

            word = Regex.Replace(word, "ough ", "Г "); //rule 26
            word = Regex.Replace(word, " their ", " ГГ "); //rule 28
            word = Regex.Replace(word, " are ", " Г "); //rule 29
            word = Regex.Replace(word, "ore ", "Г "); //rule 30

            if (!ConsonantFront(word, "re ")) //rule 31
            {
                word = Regex.Replace(word, " re ", " ГГ "); 
            }

            word = Regex.Replace(word, " be ", " Г "); //rule 32

            if (ConsonantBack(word, "ear")) //rule 33
            {
                word = Regex.Replace(word, "ear", "Г");
            }
            if (!ConsonantBack(word, "ear")) //rule 34
            {
                word = Regex.Replace(word, "ear", "ГГ");
            }

            word = Regex.Replace(word, "ous ", "Г "); //rule 35
            word = Regex.Replace(word, "ear ", "ГГ "); //rule 36
            word = Regex.Replace(word, "ing ", "Г "); //rule 37
            word = Regex.Replace(word, "eau", "Г"); //rule 38
            word = Regex.Replace(word, "eer", "ГГ"); //rule 39
            word = Regex.Replace(word, "air", "ГГ"); //rule 40
            word = Regex.Replace(word, "era", "ГГ"); //rule 41
            word = Regex.Replace(word, "ere", "ГГ"); //rule 42
            word = Regex.Replace(word, "eri", "ГГ"); //rule 43
            word = Regex.Replace(word, "ero", "ГГ"); //rule 44

            if (ConsonantFront(word, "le ")) //rule 45
            {
                if(FindLetterBeforeSubstr(word,"le ") != 's')
                word = Regex.Replace(word, "le ", "Г ");
            }
            

            word = Regex.Replace(word, "ae", "Г"); //rule 48
            word = Regex.Replace(word, "ai", "Г");
            word = Regex.Replace(word, "au", "Г");
            word = Regex.Replace(word, "ea", "Г");
            word = Regex.Replace(word, "ei", "Г");
            word = Regex.Replace(word, "eo", "Г");
            word = Regex.Replace(word, "ee", "Г");
            word = Regex.Replace(word, "eu", "Г");
            word = Regex.Replace(word, "ie", "Г");
            word = Regex.Replace(word, "oa", "Г");
            word = Regex.Replace(word, "oe", "Г");
            word = Regex.Replace(word, "oi", "Г");
            word = Regex.Replace(word, "ou", "Г");
            word = Regex.Replace(word, "oo", "Г");
            word = Regex.Replace(word, "ua", "Г");
            word = Regex.Replace(word, "ue", "Г");
            word = Regex.Replace(word, "ui", "Г");
            word = Regex.Replace(word, "uo", "Г");

            word = Regex.Replace(word, "ed ", " "); //rule 49


            if (!ConsonantFront(word, "y")) //rule 50
            {
                word = Regex.Replace(word, "y", "");
            }

            if (ContainsVowel(word)) //rule 51
            {
                word = Regex.Replace(word, "e ", " ");
            }

           


            word = FillOtherSyllables(word);

            //word = initialWord + "(" + word + ")";          Вивід слів + букв Г в дужках
            //word = initialWord;                             Вивід лише слів
                                                            //Вивід лише букв Г по замовчуванню

            word = Regex.Replace(word, " ", ""); //remove spaces before sending word back

            return word;
        }

        private bool ContainsVowel(string word) //checks all characters except for last 2 (possible e and " ")
        {
            CharactersTable check = new CharactersTable(Table.Table1);
            for(int i = 0; i < word.Length - 2; i++)
            {
                if (!check.isConsonant(word[i]) && Char.IsLetter(word[i]))
                {
                    return true;
                }
            }
            return false;
        }
        private bool ContainsVowelFull(string word) //checks all characters except for last 2 (possible e and " ")
        {
            CharactersTable check = new CharactersTable(Table.Table1);
            for (int i = 0; i < word.Length; i++)
            {
                if (!check.isConsonant(word[i]) && Char.IsLetter(word[i]))
                {
                    return true;
                }
            }
            return false;
        }

        private string FillOtherSyllables(string word)
        {
            CharactersTable check = new CharactersTable(Table.Table1);
            if (!word.Contains('Г'))
            {
                string syllables = "";
                foreach(char c in word)
                {
                    if(!check.isConsonant(c) && Char.IsLetter(c))
                    {
                        syllables += "Г";
                    }
                }
                return syllables;
            }
            else
            {
                string syllables = "";
                foreach (char c in word)
                {
                    if(c == 'Г')
                    {
                        syllables += "Г";
                    }
                }
                string[] splitWord = word.Split('Г');
                foreach (string syl in splitWord)
                {
                    foreach (char c in syl)
                    {
                        if (!check.isConsonant(c) && Char.IsLetter(c))
                        {
                            syllables += "Г";
                        }
                    }
                }
                return syllables;
            }
        }
        private char FindLetterBeforeSubstr(string word, string substring)
        {
            int index = word.IndexOf(substring) - 1;
            char c = word[index];
            return c;
        }
        private bool ConsonantFront(string word, string substring) // check if there is a consonant before substring
        {
            if (word.Contains(substring))
            {
                CharactersTable check = new CharactersTable(Table.Table1); //Consonants table
                int index = word.IndexOf(substring) - 1;
                char c = word[index];
                if (check.isConsonant(c))
                {
                    return true;
                }
            }
            return false;
        }        
        private bool ConsonantBack(string word, string substring) // check if there is a consonant after substring
        {
            if (word.Contains(substring))
            {
                CharactersTable check = new CharactersTable(Table.Table1); //Consonants table
                int index = word.IndexOf(substring) + substring.Length;
                char c = word[index];
                if (check.isConsonant(c))
                {
                    return true;
                }
            }
            return false;
        }

        #region old/bad implementation
        //public override string Process(string input, bool[] isCheckbox)
        //{
        //    var res = ProcessTwoSoundingLetters(input);
        //    res = ProcessDoubleConsonants(res);
        //    res = ReductionReplacements(res);
        //    res = AsymilativeReplacements(res);

        //    return res;
        //}
        public override string RemoveTechnicalCharacters(string word)
        {
            return word.Replace("d", "дж")
                       .Replace("z", "дз")
                       .Replace("v", "в")
                       .Replace("w", "в")
                       .Replace("u", "в")
                       .Replace("j", "й")
                       .Replace("Y", "й");
        }
      
        public override string ProcessNonStableCharacters(string word, bool isPhoneticsMode = true)
        {
            var res = base.ProcessNonStableCharacters(word);
            res = ProcessV(res);

            if (isPhoneticsMode)
                res = Regex.Replace(res, "ь", "");

            return res;
        }

        private string ReductionReplacements(string res)
        {
            res = Regex.Replace(res, "cionals", "cinals");
            res = Regex.Replace(res, "sional", "sonal");
            res = Regex.Replace(res, "tional", "tonal");
            res = Regex.Replace(res, "ions", "ons");
            res = Regex.Replace(res, "were", "wer");
            res = Regex.Replace(res, "cion", "con");
            res = Regex.Replace(res, "sion", "son");
            res = Regex.Replace(res, "ore", "or");
            res = Regex.Replace(res, "ous", "os");
            res = Regex.Replace(res, "ae", "a");
            res = Regex.Replace(res, "ai", "a");
            res = Regex.Replace(res, "au", "u");
            if (HasConsonantAKE(res))
            {
                res = Regex.Replace(res, "ake", "ak");
            }
            return res;
        }

        private string AsymilativeReplacements(string res)
        {
            res = Regex.Replace(res, "(e)(a)", "e");
            res = Regex.Replace(res, "(e)(e)", "e");
            res = Regex.Replace(res, "^(a)(r)(e)^", "ar");
            res = Regex.Replace(res, "(e)(o)", "o");
            res = Regex.Replace(res, "(i)(e)", "i");
            res = Regex.Replace(res, "(u)(e)", "e");
            res = Regex.Replace(res, "(u)(i)", "i");
            res = Regex.Replace(res, "(e)(d)^", "d");
            res = Regex.Replace(res, "(y)^", "i");
            res = Regex.Replace(res, "(e)^", "y");
            if(!HasNualsSualsTuals(res))
            {
                res = Regex.Replace(res, "(u)(a)", "u");
            }

            return res;
        }

        private string ProcessDoubleConsonants(string input)
        {
            input = Regex.Replace(input, @"([a-zA-Z])\1+", "$1");

            return input;
        }

        private string ProcessTwoSoundingLetters(string input)
        {
            input = ReplacePhoneticCharacter('ю', "jу", input);
            return input;
        }

        private string ProcessDzDj(string word)
        {
            word = word.Replace("дж", "d");

            var indexOfDz = word.IndexOf("дз");

            while (indexOfDz != -1)
            {
                if (HasPredefinedPreffix(word, indexOfDz))
                {
                    indexOfDz = word.IndexOf("дз", indexOfDz + 1);
                }
                else
                {
                    word = word.Remove(indexOfDz, 2).Insert(indexOfDz, "z");
                    indexOfDz = word.IndexOf("дз", indexOfDz + 1);
                }
            }
            return word;
        }

        public string ProcessV(string word)
        {
            var indexOfV = word.IndexOf('в');

            while (indexOfV != -1)
            {
                if (IsFirstOrHasVowelBefore(word, indexOfV) && IsLastOrHasConsonantAfter(word, indexOfV))
                {
                    word = word.Remove(indexOfV, 1).Insert(indexOfV, "u");
                }
                else if (HasConsonantBeforeAndVowelAfter(word, indexOfV))
                {
                    word = word.Remove(indexOfV, 1).Insert(indexOfV, "w");
                }
                else
                {
                    word = word.Remove(indexOfV, 1).Insert(indexOfV, "v");
                }

                indexOfV = word.IndexOf('в', indexOfV + 1);
            }

            return word;
        }

        private bool IsFirstOrHasVowelBefore(string word, int indexOfV)
        {
            return indexOfV == 0 || !CharactersTable.isConsonant(word[indexOfV - 1]);
        }

        private bool IsLastOrHasConsonantAfter(string word, int indexOfV)
        {
            return indexOfV == word.Length - 1 || CharactersTable.isConsonant(word[indexOfV + 1]);
        }

        private bool HasConsonantBeforeAndVowelAfter(string word, int indexOfV)
        {
            return (indexOfV != 0 && CharactersTable.isConsonant(word[indexOfV - 1]))
                   && (indexOfV != word.Length - 1 && !CharactersTable.isConsonant(word[indexOfV + 1]));
        }

        private bool HasPredefinedPreffix(string word, int indexOfSound)
        {
            if (indexOfSound > 1 && this.dzPrefixes.Any(p => p == word.Substring(indexOfSound - 2, p.Length)))
                return true;

            return false;
        }
        private bool HasNualsSualsTuals(string input)
        {
            if (input.Contains("nual") || input.Contains("sual") || input.Contains("tual"))
            {
                return true;
            }
            return false;
        }
        private bool HasConsonantAKE(string input)
        {
            if(input.Contains("ake"))
            {
          
                Character temp = new Character();
                temp.CharacterValue = input[input.IndexOf("ake") - 1];
                {
                    if(temp.IsConsonant)
                    {
                        return true;
                    }
                }
            }
            return false;
        }
        #endregion
    }
}
